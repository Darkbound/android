package com.example.teamsolo;

import java.util.ArrayList;
import java.util.List;

import com.example.teamsolo.Enums.mathOperatorsEnum;


public class Calculator {
	private static Calculator instance = null;
	protected Calculator() {

	}
	public static Calculator getInstance() {
		if(instance == null) {
			instance = new Calculator();
		}
		return instance;
	}

	public double calculate(String input){

		String[] numbers = input.split("[+-/*//]");
		List<mathOperatorsEnum> operators = splitOperators(input);

		double result = Integer.parseInt(numbers[0]);

		for (int i = 0; i < operators.size(); i++) {

			switch (operators.get(i))
			{
			case addition:
				result = result + Integer.parseInt(numbers[i + 1]);			
				break;
			case subtraction:
				result = result - Integer.parseInt(numbers[i + 1]);
				break;
			case multiplication:
				result = result * Integer.parseInt(numbers[i + 1]);
				break;
			case division:
				result = result / Integer.parseInt(numbers[i + 1]);
				break;
			default:
				break;
			}
		}

		return result;
	}

	private static List<mathOperatorsEnum> splitOperators(String input) {
		List<mathOperatorsEnum> operators = new ArrayList<mathOperatorsEnum>();

		for (int i = 0; i < input.length(); i++) {			
			switch (input.charAt(i))
			{
			case '+':
				operators.add(mathOperatorsEnum.addition);
				break;
			case '-':
				operators.add(mathOperatorsEnum.subtraction);
				break;
			case '*':
				operators.add(mathOperatorsEnum.multiplication);
				break;
			case '/':
				operators.add(mathOperatorsEnum.division);
				break;
			default:
				break;
			}
		}

		return operators;
	}
}
