package com.example.teamsolo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {
	private String username;
	private String password;
	private String repeatPassword;
	
	EditText usernameField;
	EditText passwordField;
	EditText repeatPasswordField;
	
	User newUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		usernameField = (EditText) findViewById(R.id.usernameRegister);
		passwordField = (EditText) findViewById(R.id.passwordRegister);
		repeatPasswordField = (EditText) findViewById(R.id.passwordRepeatRegister);
	}
	
	protected void register(View v) {
		Validator validator = Validator.getInstance();

		username = usernameField.getText().toString();
		password = passwordField.getText().toString();
		repeatPassword = repeatPasswordField.getText().toString();
		
		if (validator.username(this.username) && 
			validator.password(this.password) && 
			validator.repeatPassword(this.password, this.repeatPassword)) {
			
			newUser.createUser(this.username, this.password, this.repeatPassword);
			
		}
	}
}
