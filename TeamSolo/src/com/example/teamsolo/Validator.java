package com.example.teamsolo;

public class Validator {
	private static final int MINIMUM_PASSWORD_LENGTH = 6;
		
	private static Validator instance = null;
	
	protected Validator() {
		
	}
	public static Validator getInstance() {
		if(instance == null) {
			instance = new Validator();
		}
		return instance;
	}
	
	public boolean username(String username){
		if (username.length() >= 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean password(String password){
		boolean overSixCharsLength = false;
		boolean hasUpperCase = false;
		boolean hasSpecialCharacter = false;
		boolean hasNumber = false;
		
		if (password.length() > MINIMUM_PASSWORD_LENGTH) {
			overSixCharsLength = true;
		}

		for (int i = 0; i < password.length(); i++) {
			if (hasUpperCase == false && Character.isUpperCase(password.charAt(i))) {
				hasUpperCase = true;				
			}
			
			if (hasNumber == false && Character.isDigit(password.charAt(i))) {
				hasNumber = true;
			}
			
			if (hasSpecialCharacter == false && password.matches("[^A-Za-z0-9 ]")) {
				hasSpecialCharacter = true;
			}
			
		}
		
		if(hasUpperCase && hasSpecialCharacter && hasNumber && overSixCharsLength){
			return true;
		} else {
			return false;
		}
	}
	
	public boolean repeatPassword(String password, String repeatPassword) {
		if (password.equals(repeatPassword)) {
			return true;
		} else {
			return false;
		}
	}
	
}
