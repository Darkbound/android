package com.example.teamsolo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {
	// Numbers
	private final int numberZeroID = R.id.numberZero;
	private final int numberOneID = R.id.numberOne;
	private final int numberTwoID = R.id.numberTwo;
	private final int numberThreeID = R.id.numberThree;
	private final int numberFourID = R.id.numberFour;
	private final int numberFiveID = R.id.numberFive;
	private final int numberSixID = R.id.numberSix;
	private final int numberSevenID = R.id.numberSeven;
	private final int numberEightID = R.id.numberEight;
	private final int numberNineID = R.id.numberNine;

	// Operators
	private final int additionID = R.id.addition;
	private final int subtractionID = R.id.subtraction;
	private final int multiplicationID = R.id.multiplication;
	private final int divisionID = R.id.division;

	// Miscellaneous
	private double result;
	private String input;
	private TextView calculatorAreaTextView;	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator);

		this.input = "";
		this.calculatorAreaTextView = (TextView)findViewById(R.id.calculatorArea);
	}

	public void backspace(View v){
		if (this.input.length() > 0) {
			this.input = this.input.substring(0, this.input.length() - 1);
			this.updateInputview(this.input);
		}	
		Log.d("INPUT: ", this.input);
	}

	public void inputComma(View v){
		this.input += ".";
		this.updateInputview(this.input);
		Log.d("INPUT: ", this.input);
	}

	public void inputOperator(View v){
		switch(v.getId()) {
		case additionID:
			this.input += "+";
			break;
		case subtractionID:
			this.input += "-";
			break;
		case multiplicationID:
			this.input += "*";
			break;
		case divisionID:
			this.input += "/";
			break;
		}

<<<<<<< HEAD
		Log.d("INPUT: ", this.input);
=======
>>>>>>> 86854a48f152521ea665911f7ee2caa524d4c0a5
		this.updateInputview(this.input);
	}

	public void inputNumber(View v){
		switch(v.getId()) {
		case numberZeroID:
			this.input += "0";
			break;
		case numberOneID:
			this.input += "1";
			break;
		case numberTwoID:
			this.input += "2";
			break;
		case numberThreeID:
			this.input += "3";
			break;
		case numberFourID:
			this.input += "4";
			break;
		case numberFiveID:
			this.input += "5";
			break;
		case numberSixID:
			this.input += "6";
			break;
		case numberSevenID:
			this.input += "7";
			break;
		case numberEightID:
			this.input += "8";
			break;
		case numberNineID:
			this.input += "9";
			break;
		default:
			this.input += "0";
		}

		Log.d("INPUT: ", this.input);
		this.updateInputview(this.input);
	}

	public void updateInputview(String text){
		this.calculatorAreaTextView.setText(text);		
	}

	public void calculateResult(View v){
		if (this.input.length() > 0) {
			Log.d("INPUT: ", this.input);
			this.result = Calculator.getInstance().calculate(this.input);	
			this.updateInputview(Double.toString(this.result));
			this.input = "";
		}
	}
}
