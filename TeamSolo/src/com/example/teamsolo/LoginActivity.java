package com.example.teamsolo;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends AppCompatActivity {
	private final String USERNAME = "user";
	private final String PASSWORD = "pass";

	private EditText usernameField;
	private EditText passwordField;

	private String username;
	private String password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		usernameField = (EditText)findViewById(R.id.usernameLogin);
		passwordField = (EditText)findViewById(R.id.passwordLogin);
	}

	public void loginButton(View v) {
		username = usernameField.getText().toString();
		password = passwordField.getText().toString();

		if (username.equals(USERNAME) && password.equals(PASSWORD)) {
			Intent i = new Intent(this, CalculatorActivity.class);
			startActivity(i);
		} else {
			Toast.makeText(this, "Error: Wrong credentials, please try again!", Toast.LENGTH_LONG).show();
		}
	}

	public void registerButton(View v) {
		Intent i = new Intent(this, RegisterActivity.class);
		startActivity(i);
	}
}